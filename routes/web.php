<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Client'], function () {
    Route::get('client', 'HomeController@index')->name('client.index');


});
//
//Route::get(uri('giaovien'),function (){
//    return vi
//});
Auth::routes();
Route::get('/', function (){
    return view('welcome');
});
Route:: group(['prefix' => 'admin', 'middleware' => 'auth', 'namespace' => 'Admin'], function () {
    Route::redirect('/', 'dashboard', 301);
    Route::get('dashboard', 'HomeController@index')->name('admin.dashboard');
    Route:: group(['prefix' => 'posts', 'middleware' => 'auth'], function () {
        Route::get('/', 'PostController@index')->name('posts.index');
    });
});
