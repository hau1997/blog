<!DOCTYPE html>
<html>
<head>
    <title>Trang quản lý ĐH Công Nghiệp Hà Nội</title>
    <link rel="stylesheet" type="text/css" href="frontend/style.css">
    <script type="text/javascript" src="frontend/script.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="header">

    <div id="logo">
        <img src="frontend/img/logohaui.jpg" id="imglogo" >
        <h3><a href="index.html" style="color: #fff; text-decoration: none;">TRANG QUẢN LÝ GIẢNG DẠY - ĐHCNHN</a></h3>
    </div>
    <div id="account">
        <ul id="menu" >
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Triệu Kim Hậu
                    <span class="caret"></span></a>
                <ul class="dropdown-menu" id="uldrop">
                    <li><a href="taikhoan.html">Tài khoản</a></li>
                    <li><a href="login.html">Thoát</a></li>
                </ul>
            </li>
        </ul>
    </div>

</div>

<div class="row">
    <div class="container-fuild">
        <div class="col-md-2">
            <div id="container-left" class="dropdown">
                <ul id="menu-left">
                    <h4 style="padding: 10px;">Danh mục</h4>
                    <hr>
                    <li><a href="thongbao.html">Thông báo</a></li>
                    <li><a href="#">Giới thiệu</a></li>
                    <li class="dropdown-submenu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Danh sách</a>
                        <ul class="dropdown-menu">
                            <li><a href="lophoc.html">Lớp học</a></li>

                        </ul>
                    </li>
                    <li class="dropdown-submenu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Chia sẻ thông tin</a>
                        <ul class="dropdown-menu">
                            <li><a href="chiaselop.html">Chia sẻ trong lớp</a></li>
                            <li><a href="chiasenhatruong.html">Chia sẻ với nhà trường</a></li>
                        </ul>
                    </li>
                </ul>
                <img src="frontend/img/120nam.jpg" style="width:195px; height:150px; margin-top: 10px;">
            </div>
        </div>
        <div class="col-md-9">
            <div>
                <h2>Xin chào Hậu!!!</h2>
                <h3>Chúc bạn có một ngày làm việc hiệu quả!!!</h3>
            </div>

            <div id="content" style="margin-top: 350px;">
                <hr>
                <p style="margin-left: 50px; text-align: center;">Copyright 2018 © HaUI </p>
                <p style="text-align: center;">Phát triển hệ thống bởi khoa CNTT | KTPM2, Điện thoại: <b>0868183611</b></p>
            </div>
        </div>
    </div>
</div>
</body>
</html>