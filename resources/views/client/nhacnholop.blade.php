<!DOCTYPE html>
<html>
<head>
    <title>Trang quản lý ĐH Công Nghiệp Hà Nội</title>
    <link rel="stylesheet" type="text/css" href="frontend/style.css">
    <script type="text/javascript" src="frontend/script.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="header">

    <div id="logo">
        <img src="frontend/img/logohaui.jpg" id="imglogo" >
        <h3><a href="index.html" style="color: #fff; text-decoration: none;">TRANG QUẢN LÝ GIẢNG DẠY - ĐHCNHN</a></h3>
    </div>
    <div id="account">
        <ul id="menu" >
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Triệu Kim Hậu
                    <span class="caret"></span></a>
                <ul class="dropdown-menu" id="uldrop">
                    <li><a href="taikhoan.html">Tài khoản</a></li>
                    <li><a href="login.html">Thoát</a></li>
                </ul>
            </li>
        </ul>
    </div>

</div>

<div class="row">
    <div class="container-fuild">
        <div class="col-md-2">
            <div id="container-left" class="dropdown">
                <ul id="menu-left">
                    <h4 style="padding: 10px;">Danh mục</h4>
                    <hr>
                    <li><a href="thongbao.html">Thông báo</a></li>
                    <li><a href="#">Giới thiệu</a></li>
                    <li class="dropdown-submenu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Danh sách</a>
                        <ul class="dropdown-menu">
                            <li><a href="lophoc.html">Lớp học</a></li>

                        </ul>
                    </li>
                    <li class="dropdown-submenu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Chia sẻ thông tin</a>
                        <ul class="dropdown-menu">
                            <li><a href="chiaselop.html">Chia sẻ trong lớp</a></li>
                            <li><a href="chiasenhatruong.html">Chia sẻ với nhà trường</a></li>
                        </ul>
                    </li>
                </ul>
                <img src="frontend/img/120nam.jpg" style="width:195px; height:150px; margin-top: 10px;">
            </div>
        </div>
        <div class="col-md-10">
            <h2>Nhắc nhở</h2>
            <hr>
            <div>
                <select class="form-control">
                    <option>Tuần 1</option>
                    <option>Tuần 2</option>
                </select>
                <hr>
                <div class="row">
                    <div class="col-md-8">
                        <h3>Danh sách nhóm</h3>
                        <table class="table table-striped table-inverse table-hover">
                            <thead>
                            <tr>
                                <th>Nhóm</th>
                                <th>Nhắc nhở</th>
                                <th>Tiến độ</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>
                                    <textarea class="form-control" rows="3" id="comment"></textarea>
                                </td>
                                <td>
                                    <input type="text" name="" style="width: 50px; margin-top: 25px"> %
                                </td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>
                                    <textarea class="form-control" rows="3" id="comment"></textarea>
                                </td>
                                <td>
                                    <input type="text" name="" style="width: 50px;margin-top: 25px">%
                                </td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>
                                    <textarea class="form-control" rows="3" id="comment"></textarea>
                                </td>
                                <td>
                                    <input type="text" name="" style="width: 50px; margin-top: 25px">%
                                </td>
                            </tr>
                            <td>4</td>
                            <td>
                                <textarea class="form-control" rows="3" id="comment"></textarea>
                            </td>
                            <td>
                                <input type="text" name="" style="width: 50px; margin-top: 25px"> %
                            </td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>
                                    <textarea class="form-control" rows="3" id="comment"></textarea>
                                </td>
                                <td>
                                    <input type="text" name="" style="width: 50px;margin-top: 25px;">%
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <hr>
                        <a class="btn btn-secondary" style="margin-top: 3px" href="chitietlh.html" role="button">Close</a>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                    <div class="col-md-4">
                        <h3>Danh sách sinh viên</h3>
                        <table class="table table-striped table-inverse table-hover">
                            <thead>
                            <tr>
                                <th>Họ tên</th>
                                <th>Nhóm</th>
                                <th>Đã xem</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Nguyễn Đăng Đức</td>
                                <td>1</td>
                                <td>
                                    <input type="checkBox" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td>Triệu kim hậu</td>
                                <td>2</td>
                                <td>
                                    <input type="checkBox" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td>Nguyễn Văn A</td>
                                <td>3</td>
                                <td>
                                    <input type="checkBox" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td>Nguyễn Đăng Đức</td>
                                <td>1</td>
                                <td>
                                    <input type="checkBox" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td>Triệu kim hậu</td>
                                <td>2</td>
                                <td>
                                    <input type="checkBox" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td>Nguyễn Văn A</td>
                                <td>3</td>
                                <td>
                                    <input type="checkBox" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td>Nguyễn Đăng Đức</td>
                                <td>1</td>
                                <td>
                                    <input type="checkBox" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td>Triệu kim hậu</td>
                                <td>2</td>
                                <td>
                                    <input type="checkBox" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td>Nguyễn Văn A</td>
                                <td>3</td>
                                <td>
                                    <input type="checkBox" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td>Nguyễn Đăng Đức</td>
                                <td>1</td>
                                <td>
                                    <input type="checkBox" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td>Triệu kim hậu</td>
                                <td>2</td>
                                <td>
                                    <input type="checkBox" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td>Nguyễn Văn A</td>
                                <td>3</td>
                                <td>
                                    <input type="checkBox" disabled>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div id="content" style="margin-top: 450px;">
                <hr>
                <p style="margin-left: 140px; text-align: center;">Copyright 2018 © HaUI </p>
                <p style="text-align: center;">Phát triển hệ thống bởi khoa CNTT | KTPM2, Điện thoại: <b>0868183611</b></p>
            </div>
        </div>
    </div>
</div>
</body>
</html>