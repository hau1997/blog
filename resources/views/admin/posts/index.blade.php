@extends('layouts.admin')
@section('content')
    <h3>LỚP ỔN ĐỊNH</h3>
    <hr>
    <div class="col-md-12">
        <table class="table table-striped table-inverse table-hover">
            <thead>
            <tr>
                <th>STT</th>
                <th>Mã lớp</th>
                <th>Tên lớp</th>
                <th>Sĩ số</th>
                <th>Hành động</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>1</td>
                <td>SV2301</td>
                <td>Kim Hậu</td>
                <td>Admin</td>
                <td>
                    <a href="#">
                        <span class="glyphicon glyphicon-plus btn btn-primary" style="margin-right:10px;"></span>
                    </a>
                    <a href="#">
                        <span class="glyphicon glyphicon-pencil btn btn-primary" style="margin-right:10px;"></span>
                    </a>
                    <a href="#">
                        <span class="glyphicon glyphicon-trash btn btn-primary" style="margin-right:10px;"></span>
                    </a>
                </td>
            </tr>
            <tr>
                <td>1</td>
                <td>SV2301</td>
                <td>Kim Hậu</td>
                <td>Admin</td>
                <td>
                    <a href="#">
                        <span class="glyphicon glyphicon-plus btn btn-primary" style="margin-right:10px;"></span>
                    </a>
                    <a href="#">
                        <span class="glyphicon glyphicon-pencil btn btn-primary" style="margin-right:10px;"></span>
                    </a>
                    <a href="#">
                        <span class="glyphicon glyphicon-trash btn btn-primary" style="margin-right:10px;"></span>
                    </a>
                </td>
            </tr>
            <tr>
                <td>1</td>
                <td>SV2301</td>
                <td>Kim Hậu</td>
                <td>Admin</td>
                <td>
                    <a href="#">
                        <span class="glyphicon glyphicon-plus btn btn-primary" style="margin-right:10px;"></span>
                    </a>
                    <a href="#">
                        <span class="glyphicon glyphicon-pencil btn btn-primary" style="margin-right:10px;"></span>
                    </a>
                    <a href="#">
                        <span class="glyphicon glyphicon-trash btn btn-primary" style="margin-right:10px;"></span>
                    </a>
                </td>
            </tr>
            <tr>
                <td>1</td>
                <td>SV2301</td>
                <td>Kim Hậu</td>
                <td>Admin</td>
                <td>
                    <a href="#">
                        <span class="glyphicon glyphicon-plus btn btn-primary" style="margin-right:10px;"></span>
                    </a>
                    <a href="#">
                        <span class="glyphicon glyphicon-pencil btn btn-primary" style="margin-right:10px;"></span>
                    </a>
                    <a href="#">
                        <span class="glyphicon glyphicon-trash btn btn-primary" style="margin-right:10px;"></span>
                    </a>
                </td>
            </tr>
            <tr>
                <td>1</td>
                <td>SV2301</td>
                <td>Kim Hậu</td>
                <td>Admin</td>
                <td>
                    <a href="#">
                        <span class="glyphicon glyphicon-plus btn btn-primary" style="margin-right:10px;"></span>
                    </a>
                    <a href="#">
                        <span class="glyphicon glyphicon-pencil btn btn-primary" style="margin-right:10px;"></span>
                    </a>
                    <a href="#">
                        <span class="glyphicon glyphicon-trash btn btn-primary" style="margin-right:10px;"></span>
                    </a>
                </td>
            </tr>
            </tbody>
        </table>
        <a class="btn btn-default" style="margin-top: 3px" href="starter.html" role="button">Back to Home Page</a>
    </div>
@endsection